return {
  "mistricky/code-link.nvim",
  config = {
    copy_command = function(link)
      vim.fn.setreg("+", link)
      if vim.fn.executable("pbcopy") then
        return 'echo "' .. link .. '" | pbcopy'
      else
        return 'echo "' .. link .. '" '
      end
    end,
  },
}
